package com.paic.arch.jmsbroker;

import com.paic.arch.jmsbroker.impl.ActionMQBrokerService;

/**
 * @Title：JmsMessageBrokerSupport
 * @author：boge【www.xboge.com】
 * @date：2018/2/27-下午6:38 
 * @email：boge.x@foxmail.com
 * @Description: 通过接口实现具体实现类的解耦，另外针对默认静态方法，返回默认实现，具体思路麻烦请查阅feedback.md
 */
public interface JmsMessageBrokerSupport {

    /**
     *在新的重构中，该方法只是为了支持向下兼容，不建议使用
     */
    @Deprecated()
    static JmsMessageBrokerSupport createARunningEmbeddedBrokerOnAvailablePort() throws Exception {
        return new ActionMQBrokerService().startService();
    }
    @Deprecated()
    static JmsMessageBrokerSupport bindToBrokerAtUrl(String aBrokerUrl) throws Exception {
        return new ActionMQBrokerService(aBrokerUrl);
    }
    /**
     * 返回实现服务的url，
     */
    @Deprecated()
    String getBrokerUrl();

    /**
     * 实在不太了解为啥各个方法已经返回this的情况下为啥需要and(),andThen等方法
     */
    default JmsMessageBrokerSupport andThen() {
        return this;
    }

    /**
     * 启动嵌入式broker服务,如果项目中使用独立服务，则直接返回即可
     * 该用法主要结合下面stopTheRunningBroker使用，务必进行关闭
     */
    JmsMessageBrokerSupport startService() throws Exception;

    /**
     * 关闭MQ服务
     */
    void stopTheRunningBroker() throws Exception;

    /**
     * 兼容旧版本接口
     */
    JmsMessageBrokerSupport sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend);
    long getEnqueuedMessageCountAt(String aDestinationName) throws Exception;
    String retrieveASingleMessageFromTheDestination(String aDestinationName);
    String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout);

    /**
     * 新版流式接口，其实对具体业务还得根据业务进行提炼，但思路就是这样，我觉得不需要通过bindToActiveMqBrokerAt（）
     * 方法进行各种具体实现类的调用，不然实现类多类可是个问题，本该使用工厂方法模式进行不同类处理，不过好像单单这个项目只是实现也行
     * 设置发送消息内容
     */
    JmsMessageBrokerSupport sendTheMessage(String inputMessage);
    /**
     * 设置消息队列queue
     */
    JmsMessageBrokerSupport to(String inputQueue);
    /**
     * 执行发送操作
     */
    JmsMessageBrokerSupport execute();

    /**
     * 本想把该类单独提炼出来，方便以后扩张各种Exception，但是在测试代码中已经使用，也就是必须兼容老旧代码
     */
    class NoMessageReceivedException extends RuntimeException {
        public NoMessageReceivedException(String reason) {
            super(reason);
        }
    }
}
