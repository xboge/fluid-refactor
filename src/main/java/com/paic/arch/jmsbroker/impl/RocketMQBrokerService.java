package com.paic.arch.jmsbroker.impl;

import com.paic.arch.jmsbroker.JmsMessageBrokerSupport;

/**
 * @version V1.0
 * @package：com.paic.arch.jmsbroker.impl
 * @author：boge【www.xboge.com】
 * @date：2018/2/27-下午10:26
 * @email：boge.x@foxmail.com
 * @Description: 新增rocketmq的MQ实现
 */
public class RocketMQBrokerService implements JmsMessageBrokerSupport {
    @Override
    public String getBrokerUrl() {
        return null;
    }

    @Override
    public JmsMessageBrokerSupport startService() throws Exception {
        return null;
    }

    @Override
    public void stopTheRunningBroker() throws Exception {

    }

    @Override
    public JmsMessageBrokerSupport sendATextMessageToDestinationAt(String aDestinationName, String aMessageToSend) {
        return null;
    }

    @Override
    public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
        return 0;
    }

    @Override
    public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
        return null;
    }

    @Override
    public String retrieveASingleMessageFromTheDestination(String aDestinationName, int aTimeout) {
        return null;
    }

    @Override
    public JmsMessageBrokerSupport sendTheMessage(String inputMessage) {
        return null;
    }

    @Override
    public JmsMessageBrokerSupport to(String inputQueue) {
        return null;
    }

    @Override
    public JmsMessageBrokerSupport execute() {
        return null;
    }
}
