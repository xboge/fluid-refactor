package com.paic.arch.jmsbroker.impl;

import com.paic.arch.jmsbroker.JmsMessageBrokerSupport;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.DestinationStatistics;
import org.slf4j.Logger;

import javax.jms.*;

import java.lang.IllegalStateException;

import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @version V1.0
 * @package：com.paic.arch.jmsbroker.mq
 * @author：boge【www.xboge.com】
 * @date：2018/2/27-下午5:08
 * @email：boge.x@foxmail.com
 * @Description: ActionMQ实现的brokerservice服务类
 */
public class ActionMQBrokerService implements JmsMessageBrokerSupport {
    private static final Logger LOG = getLogger(ActionMQBrokerService.class);
    private static final int ONE_SECOND = 1000;
    private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;
    public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";

    private String inputMessage;
    private String inputQueue;
    private String brokerUrl;
    private BrokerService brokerService;

    public ActionMQBrokerService(){
        this.brokerUrl = DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000);
    }
    public ActionMQBrokerService(String brokerUrl){
        this.brokerUrl = brokerUrl;
    }

    @Override
    public String getBrokerUrl() {
        return this.brokerUrl;
    }

    @Override
    public JmsMessageBrokerSupport startService() throws Exception{
        LOG.debug("Creating a new broker at {}", this.brokerUrl);
        createEmbeddedBroker();
        startEmbeddedBroker();
        return this;
    }

    @Override
    public void stopTheRunningBroker() throws Exception {
        if (brokerService == null) {
            throw new IllegalStateException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
        brokerService.stop();
        brokerService.waitUntilStopped();
    }

    @Override
    public JmsMessageBrokerSupport sendATextMessageToDestinationAt(String aDestinationName, String aMessageToSend) {
        executeCallbackAgainstRemoteBroker(brokerUrl, aDestinationName, (aSession, aDestination) -> {
            MessageProducer producer = aSession.createProducer(aDestination);
            producer.send(aSession.createTextMessage(aMessageToSend));
            producer.close();
            return "";
        });
        return this;
    }

    @Override
    public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
        return getDestinationStatisticsFor(aDestinationName).getMessages().getCount();
    }

    @Override
    public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
        return retrieveASingleMessageFromTheDestination(aDestinationName, DEFAULT_RECEIVE_TIMEOUT);
    }

    @Override
    public String retrieveASingleMessageFromTheDestination(String aDestinationName, int aTimeout) {
        return executeCallbackAgainstRemoteBroker(brokerUrl, aDestinationName, (aSession, aDestination) -> {
            MessageConsumer consumer = aSession.createConsumer(aDestination);
            Message message = consumer.receive(aTimeout);
            if (message == null) {
                throw new NoMessageReceivedException(String.format("No messages received from the broker within the %d timeout", aTimeout));
            }
            consumer.close();
            return ((TextMessage) message).getText();
        });
    }

    @Override
    public JmsMessageBrokerSupport sendTheMessage(String inputMessage) {
        this.inputMessage = inputMessage;
        return this;
    }

    @Override
    public JmsMessageBrokerSupport to(String inputQueue) {
        this.inputQueue = inputQueue;
        return this;
    }

    @Override
    public JmsMessageBrokerSupport execute() {
        if(null == this.inputMessage){
            throw new RuntimeException("inputMessage can't be null");
        }
        if(null == this.inputQueue){
            throw new RuntimeException("inputQueue can't be null");
        }

        return sendATextMessageToDestinationAt(inputQueue,inputMessage);
    }

    /**********************private method area*************************/

    /**
     * 返回broker服务，如果嵌入式则new对象，如果独立服务，则由系统依赖注入
     */
    private void createEmbeddedBroker() throws Exception {
        brokerService = new BrokerService();
        brokerService.setPersistent(false);
        brokerService.addConnector(brokerUrl);
    }

    private void startEmbeddedBroker() throws Exception {
        brokerService.start();
    }

    interface JmsCallback {
        String performJmsFunction(Session aSession, Destination aDestination) throws JMSException;
    }

    private String executeCallbackAgainstRemoteBroker(String aBrokerUrl, String aDestinationName, JmsCallback aCallback) {
        Connection connection = null;
        String returnValue = "";
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(aBrokerUrl);
            connection = connectionFactory.createConnection();
            connection.start();
            returnValue = executeCallbackAgainstConnection(connection, aDestinationName, aCallback);
        } catch (JMSException jmse) {
            LOG.error("failed to create connection to {}", aBrokerUrl);
            throw new IllegalStateException(jmse);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close connection to broker at []", aBrokerUrl);
                    throw new IllegalStateException(jmse);
                }
            }
        }
        return returnValue;
    }

    private String executeCallbackAgainstConnection(Connection aConnection, String aDestinationName, JmsCallback aCallback) {
        Session session = null;
        try {
            session = aConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue(aDestinationName);
            return aCallback.performJmsFunction(session, queue);
        } catch (JMSException jmse) {
            LOG.error("Failed to create session on connection {}", aConnection);
            throw new IllegalStateException(jmse);
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close session {}", session);
                    throw new IllegalStateException(jmse);
                }
            }
        }
    }

    private DestinationStatistics getDestinationStatisticsFor(String aDestinationName) throws Exception {
        Broker regionBroker = brokerService.getRegionBroker();
        for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
            if (destination.getName().equals(aDestinationName)) {
                return destination.getDestinationStatistics();
            }
        }
        throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", aDestinationName, brokerUrl));
    }
}
