### Candidate Chinese Name:
* 
 
- - -  
### Please write down some feedback about the question(could be in Chinese):
* 

- - -

坦诚说明一下，针对该题目的了解不是很到位，所以重构上总觉得缺乏清晰规划！
由于原接口使用类不带参数的静态方法，而且必须向下兼容，所以在JmsMessageBrokerSupport接口上使用静态方法提供默认实现类
本来思考，在具体实现类方面，不论是扩张中根据接口依赖注入，还是手动初始化，都应使用工厂方法模式进行调用，但又觉得单单通过接口就可解决本题目的问题
另外JmsMessageBrokerSupport.bindToBrokerAtUrl，针对参数aBrokerUrl，也觉得很奇怪，由于实现类在初始化时该值已经确定，
个人认为不需要由外部提供，而且各种MQ的实现类url不见得格式都一样，况且仅仅通过url无法识别具体实现类
而通过bindToActiveMqBrokerAt，bindToIbmMqBrokerAt，bindToTibcoMqBrokerAt方法名进行识别不是一种良好的设计模式
很抱歉，不是很清楚出题者的意图，以及重构的方向
